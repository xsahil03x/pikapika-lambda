package com.floydwiz.pikapikalambda.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@DynamoDBTable(tableName = "PikaPikaUsers")
public class User {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("childName")
    @Expose
    private String childName;
    @SerializedName("childAge")
    @Expose
    private String childAge;
    @SerializedName("parentEmail")
    @Expose
    private String parentEmail;

    public User() {
    }

    public User(String userId, String childName, String childAge, String parentEmail) {
        this.userId = userId;
        this.childName = childName;
        this.childAge = childAge;
        this.parentEmail = parentEmail;
    }

    @DynamoDBHashKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @DynamoDBAttribute
    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    @DynamoDBAttribute
    public String getChildAge() {
        return childAge;
    }

    public void setChildAge(String childAge) {
        this.childAge = childAge;
    }

    @DynamoDBAttribute
    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }
}