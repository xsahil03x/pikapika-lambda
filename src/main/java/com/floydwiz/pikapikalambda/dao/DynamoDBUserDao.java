package com.floydwiz.pikapikalambda.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.floydwiz.pikapikalambda.domain.User;
import com.floydwiz.pikapikalambda.manager.DynamoDBManager;

public class DynamoDBUserDao implements UserDao {

    private static final DynamoDBMapper mapper = DynamoDBManager.mapper();

    private static volatile DynamoDBUserDao instance;

    private DynamoDBUserDao() {
    }

    public static DynamoDBUserDao getInstance() {
        if (instance == null) {
            synchronized (DynamoDBUserDao.class) {
                if (instance == null)
                    instance = new DynamoDBUserDao();
            }
        }
        return instance;
    }

    @Override
    public void saveNewUser(User user) {
        mapper.save(user);
    }

    @Override
    public User fetchUserById(String userId) {
        return mapper.load(User.class, userId);
    }
}