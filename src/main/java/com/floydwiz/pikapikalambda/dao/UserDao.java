package com.floydwiz.pikapikalambda.dao;

import com.floydwiz.pikapikalambda.domain.User;

public interface UserDao {

    User fetchUserById(String userId);

    void saveNewUser(User user);
}