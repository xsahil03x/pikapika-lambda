package com.floydwiz.pikapikalambda.manager;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.floydwiz.pikapikalambda.util.Consts;

public class DynamoDBManager {

    private static volatile DynamoDBManager instance;

    private static DynamoDBMapper mapper;

    private DynamoDBManager() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(
                System.getenv(Consts.ENV_ACCESS_KEY_ALIAS),
                System.getenv(Consts.ENV_SECRET_KEY_ALIAS)
        );
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.AP_SOUTH_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials));
        AmazonDynamoDB amazonDynamoDB = builder.build();
        mapper = new DynamoDBMapper(amazonDynamoDB);
    }

    private static DynamoDBManager instance() {
        if (instance == null) {
            synchronized (DynamoDBManager.class) {
                if (instance == null)
                    instance = new DynamoDBManager();
            }
        }
        return instance;
    }

    public static DynamoDBMapper mapper() {
        DynamoDBManager manager = instance();
        return manager.mapper;
    }
}
