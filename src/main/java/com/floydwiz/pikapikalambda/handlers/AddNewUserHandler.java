package com.floydwiz.pikapikalambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.floydwiz.pikapikalambda.dao.DynamoDBUserDao;
import com.floydwiz.pikapikalambda.dao.UserDao;
import com.floydwiz.pikapikalambda.domain.User;
import com.floydwiz.pikapikalambda.util.ApiGatewayResponse;
import org.apache.log4j.Logger;

import java.util.Collections;

public class AddNewUserHandler implements RequestHandler<User, ApiGatewayResponse> {

    private static final Logger log = Logger.getLogger(AddNewUserHandler.class);

    @Override
    public ApiGatewayResponse handleRequest(User user, Context context) {
        try {
            UserDao userDao = DynamoDBUserDao.getInstance();
            userDao.saveNewUser(user);
            return ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(user)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & Serverless"))
                    .build();

        } catch (Exception e) {
            log.error("Error in saving user: " + e.getMessage());
            return ApiGatewayResponse.builder()
                    .setStatusCode(500)
                    .setObjectBody(e.toString())
                    .setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & Serverless"))
                    .build();
        }
    }
}
