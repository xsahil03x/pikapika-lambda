package com.floydwiz.pikapikalambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.floydwiz.pikapikalambda.dao.DynamoDBUserDao;
import com.floydwiz.pikapikalambda.dao.UserDao;
import com.floydwiz.pikapikalambda.domain.User;
import com.floydwiz.pikapikalambda.util.ApiGatewayResponse;
import com.floydwiz.pikapikalambda.util.UserIdInput;
import org.apache.log4j.Logger;

import java.util.Collections;

public class FetchUserByIdHandler implements RequestHandler<UserIdInput, ApiGatewayResponse> {

    private static final Logger log = Logger.getLogger(FetchUserByIdHandler.class);

    @Override
    public ApiGatewayResponse handleRequest(UserIdInput user, Context context) {
        try {
            UserDao userDao = DynamoDBUserDao.getInstance();
            User fetchedUser = userDao.fetchUserById(user.getUserId());
            return ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(fetchedUser)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & Serverless"))
                    .build();

        } catch (Exception e) {
            log.error("Error in saving user: " + e.getMessage());
            return ApiGatewayResponse.builder()
                    .setStatusCode(500)
                    .setObjectBody(e.toString())
                    .setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & Serverless"))
                    .build();
        }
    }
}
