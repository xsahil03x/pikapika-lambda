package com.floydwiz.pikapikalambda.util;

import java.time.Instant;

public class TimeUtil {

    public static long getTtl() {
        long now = Instant.now().getEpochSecond(); // unix time
        long ttl = 60 * 60 * 24 * 7; // 1 week in sec
        return (ttl + now); // when object will be expired
    }

}
