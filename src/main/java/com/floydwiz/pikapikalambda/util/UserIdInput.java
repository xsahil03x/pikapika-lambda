package com.floydwiz.pikapikalambda.util;

public class UserIdInput {

    private String userId;

    public UserIdInput() {
    }

    public UserIdInput(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
