package com.floydwiz.pikapikalambda.util;

public class Consts {

    public static final String ENV_ACCESS_KEY_ALIAS = "ACCESS_KEY";
    public static final String ENV_SECRET_KEY_ALIAS = "SECRET_KEY";

}
